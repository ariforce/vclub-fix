<?php

##################################################################
#
#
# This script require ffmpeg (ffprobe) and shell_exec()
#
#
##################################################################

// config section

$host='localhost';
$user = 'stalker';
$password = '1';
$db = 'stalker_db';
$charset = 'utf8';

##################################################################

function lang_iso639_2t_to_1( $iso639_2t ) {
    static $language_codes = array(
'aar'=>'aa', // Afar
'abk'=>'ab', // Abkhazian
'afr'=>'af', // Afrikaans
'aka'=>'ak', // Akan
'amh'=>'am', // Amharic
'ara'=>'ar', // Arabic
'arg'=>'an', // Aragonese
'asm'=>'as', // Assamese
'ava'=>'av', // Avaric
'aym'=>'ay', // Aymara
'aze'=>'az', // Azerbaijani
'bak'=>'ba', // Bashkir
'bam'=>'bm', // Bambara
'bel'=>'be', // Belarusian
'ben'=>'bn', // Bengali
'bis'=>'bi', // Bislama
'bos'=>'bs', // Bosnian
'bre'=>'br', // Breton
'bul'=>'bg', // Bulgarian
'cat'=>'ca', // Catalan
'ces'=>'cs', // Czech
'cha'=>'ch', // Chamorro
'che'=>'ce', // Chechen
'chv'=>'cv', // Chuvash
'cor'=>'kw', // Cornish
'cos'=>'co', // Corsican
'cre'=>'cr', // Cree
'cym'=>'cy', // Welsh
'dan'=>'da', // Danish
'deu'=>'de', // German
'div'=>'dv', // Dhivehi
'dzo'=>'dz', // Dzongkha
'ell'=>'el', // Modern Greek (1453-)
'eng'=>'en', // English
'est'=>'et', // Estonian
'eus'=>'eu', // Basque
'ewe'=>'ee', // Ewe
'fao'=>'fo', // Faroese
'fas'=>'fa', // Persian
'fij'=>'fj', // Fijian
'fin'=>'fi', // Finnish
'fra'=>'fr', // French
'fry'=>'fy', // Western Frisian
'ful'=>'ff', // Fulah
'ger'=>'de', // German bibliographic
'gla'=>'gd', // Scottish Gaelic
'gle'=>'ga', // Irish
'glg'=>'gl', // Galician
'glv'=>'gv', // Manx
'grn'=>'gn', // Guarani
'guj'=>'gu', // Gujarati
'hat'=>'ht', // Haitian
'hau'=>'ha', // Hausa
'heb'=>'he', // Hebrew
'her'=>'hz', // Herero
'hin'=>'hi', // Hindi
'hmo'=>'ho', // Hiri Motu
'hrv'=>'hr', // Croatian
'hun'=>'hu', // Hungarian
'hye'=>'hy', // Armenian
'ibo'=>'ig', // Igbo
'iii'=>'ii', // Sichuan Yi
'iku'=>'iu', // Inuktitut
'ind'=>'id', // Indonesian
'ipk'=>'ik', // Inupiaq
'isl'=>'is', // Icelandic
'ita'=>'it', // Italian
'jav'=>'jv', // Javanese
'jpn'=>'ja', // Japanese
'kal'=>'kl', // Kalaallisut
'kan'=>'kn', // Kannada
'kas'=>'ks', // Kashmiri
'kat'=>'ka', // Georgian
'kau'=>'kr', // Kanuri
'kaz'=>'kk', // Kazakh
'khm'=>'km', // Central Khmer
'kik'=>'ki', // Kikuyu
'kin'=>'rw', // Kinyarwanda
'kir'=>'ky', // Kirghiz
'kom'=>'kv', // Komi
'kon'=>'kg', // Kongo
'kor'=>'ko', // Korean
'kua'=>'kj', // Kuanyama
'kur'=>'ku', // Kurdish
'lao'=>'lo', // Lao
'lav'=>'lv', // Latvian
'lim'=>'li', // Limburgan
'lin'=>'ln', // Lingala
'lit'=>'lt', // Lithuanian
'ltz'=>'lb', // Luxembourgish
'lub'=>'lu', // Luba-Katanga
'lug'=>'lg', // Ganda
'mah'=>'mh', // Marshallese
'mal'=>'ml', // Malayalam
'mar'=>'mr', // Marathi
'mkd'=>'mk', // Macedonian
'mlg'=>'mg', // Malagasy
'mlt'=>'mt', // Maltese
'mon'=>'mn', // Mongolian
'mri'=>'mi', // Maori
'msa'=>'ms', // Malay (macrolanguage)
'mya'=>'my', // Burmese
'nau'=>'na', // Nauru
'nav'=>'nv', // Navajo
'nbl'=>'nr', // South Ndebele
'nde'=>'nd', // North Ndebele
'ndo'=>'ng', // Ndonga
'nep'=>'ne', // Nepali
'nld'=>'nl', // Dutch
'nno'=>'nn', // Norwegian Nynorsk
'nob'=>'nb', // Norwegian BokmÎl
'nor'=>'no', // Norwegian
'nya'=>'ny', // Nyanja
'oci'=>'oc', // Occitan (post 1500)
'oji'=>'oj', // Ojibwa
'ori'=>'or', // Oriya
'orm'=>'om', // Oromo
'oss'=>'os', // Ossetian
'pan'=>'pa', // Panjabi
'pol'=>'pl', // Polish
'por'=>'pt', // Portuguese
'pus'=>'ps', // Pushto
'que'=>'qu', // Quechua
'roh'=>'rm', // Romansh
'ron'=>'ro', // Romanian
'run'=>'rn', // Rundi
'rus'=>'ru', // Russian
'sag'=>'sg', // Sango
'sin'=>'si', // Sinhala
'slk'=>'sk', // Slovak
'slv'=>'sl', // Slovenian
'sme'=>'se', // Northern Sami
'smo'=>'sm', // Samoan
'sna'=>'sn', // Shona
'snd'=>'sd', // Sindhi
'som'=>'so', // Somali
'sot'=>'st', // Southern Sotho
'spa'=>'es', // Spanish
'sqi'=>'sq', // Albanian
'srd'=>'sc', // Sardinian
'srp'=>'sr', // Serbian
'ssw'=>'ss', // Swati
'sun'=>'su', // Sundanese
'swa'=>'sw', // Swahili
'swe'=>'sv', // Swedish
'tah'=>'ty', // Tahitian
'tam'=>'ta', // Tamil
'tat'=>'tt', // Tatar
'tel'=>'te', // Telugu
'tgk'=>'tg', // Tajik
'tgl'=>'tl', // Tagalog
'tha'=>'th', // Thai
'tir'=>'ti', // Tigrinya
'ton'=>'to', // Tonga (Tonga Islands)
'tsn'=>'tn', // Tswana
'tso'=>'ts', // Tsonga
'tuk'=>'tk', // Turkmen
'tur'=>'tr', // Turkish
'twi'=>'tw', // Twi
'uig'=>'ug', // Uighur
'ukr'=>'uk', // Ukrainian
'urd'=>'ur', // Urdu
'uzb'=>'uz', // Uzbek
'ven'=>'ve', // Venda
'vie'=>'vi', // Vietnamese
'wln'=>'wa', // Walloon
'wol'=>'wo', // Wolof
'xho'=>'xh', // Xhosa
'yid'=>'yi', // Yiddish
'yor'=>'yo', // Yoruba
'zha'=>'za', // Zhuang
'zho'=>'zh', // Chinese
'zul'=>'zu', // Zulu
'und'=>'en'
);
    return $language_codes[$iso639_2t];
}

function vres2q($vres){
    static $re2q=array(
	'240'=>'1',
	'320'=>'2',
	'480'=>'3',
	'576'=>'4',
	'720'=>'5',
	'1080'=>'6',
	'2160'=>'7'
);
    return $re2q[$vres];
}

function getres($vres) {
    $arr=array(240,320,480,576,720,1080,2160);
    $closest = null;
    foreach ($arr as $item) {
      if ($closest === null || abs($vres - $closest) > abs($item - $vres)) {
         $closest = $item;
      }
   }
   return $closest;
}

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);

$pdo = new PDO($dsn, $user, $password);

$video = $pdo->query("SELECT id,rtsp_url,added FROM video WHERE rtsp_url<>'' AND status=1");

while ($row = $video->fetch())
{
    $lang='';
    $url=str_replace("ffmpeg ", "", $row['rtsp_url']);

    $video_info = json_decode(shell_exec("ffprobe -v quiet -print_format json -show_streams \"".$url."\""));

    if (isset($video_info->streams)){
	foreach ($video_info->streams as $strm){
	    if($strm->codec_type=='video'){
		$vres=$strm->height;
	    }
	    if($strm->codec_type=='audio'){
		if (isset($strm->tags->language)){
		    $lang[]=lang_iso639_2t_to_1($strm->tags->language);
		}
		else{
		    $lang[]='en';
		}
	    }
	}

    }
    else{
	$vres='720';
	    if (strpos($row['rtsp_url'], 'gujrati')){
		$lang[]=lang_iso639_2t_to_1('guj');
	    }
	    elseif (strpos($row['rtsp_url'], 'mala')){
		$lang[]=lang_iso639_2t_to_1('mal');
	    }
	    elseif (strpos($row['rtsp_url'], 'telegu')){
		$lang[]=lang_iso639_2t_to_1('tel');
	    }
	    elseif (strpos($row['rtsp_url'], 'bangla')){
		$lang[]=lang_iso639_2t_to_1('ben');
	    }
	    elseif (strpos($row['rtsp_url'], 'punjabi')){
		$lang[]=lang_iso639_2t_to_1('pan');
	    }
	    elseif (strpos($row['rtsp_url'], 'tamil')){
		$lang[]=lang_iso639_2t_to_1('tam');
	    }
	    elseif (strpos($row['rtsp_url'], 'hindi')){
		$lang[]=lang_iso639_2t_to_1('hin');
	    }
	    else{
		$lang[]=lang_iso639_2t_to_1('eng');
	    }
    }

    $sql = "INSERT IGNORE INTO video_series_files (video_id,file_type,protocol,url,languages,quality,accessed,status,date_add,date_modify) values (?,'video','custom',?,?,?,'1','1',?,NOW()) ";
    $sth = $pdo->prepare($sql);
    $sth->execute(array($row['id'],$row['rtsp_url'],serialize($lang),vres2q(getres($vres)),$row['added']));

	echo "video_id:".$row['id'].", url:".$row['rtsp_url'].", languages:".serialize($lang).", quality:".vres2q(getres($vres)).";\r\n";

    
}


?>